/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React,{useState} from 'react';
import {submitData} from '../api/index';

const Home = () => {
    const [fuid,setFuid] = useState('')
    const [redeemCode,setRedeemCode] = useState('')
    const [verificationCode,setVerificationCode] = useState('')
    const handleSubmit = async()=>{
        const data = await submitData({fuid,redeemCode,verificationCode})
        console.log(data)
    }
    return (

    <>
        <div className="wrap">
            <div className="container">
                <div className="head pr">
                    <h1 className="hide">REDEMPTION CENTER</h1>
                    <div className="logo"></div>
                    <div className="slogan slogan1" id="logoStyle"></div>
                    <div className="code_ct">
                            <ul className="code_list c">
                                <li className="mobile_label"><span>UID</span></li>
                                <li>
                                    <span className="desktop_label">UID</span>
                                    <input id="F_UID" tabIndex="1" type="number" placeholder="UID" onChange={(e)=>setFuid(e.target.value)} className="input_text" value={fuid}/>
                                </li>
                                <li className="mobile_label"><span id="label_titlie">Redeem Code</span></li>
                                <li>
                                    <span id="label_titlie" className="desktop_label">Redeem Code</span>
                                    <input id="F_CDKEY" tabIndex="2" type="text" placeholder="Redeem Code" value={redeemCode} onChange={(e)=>setRedeemCode(e.target.value)} className="input_text" />
                                </li>
                                <li className="mobile_label"><span id="label_vercode">Verification</span></li>
                                <li className="verification_row">
                                    <span id="label_vercode" className="desktop_label">Verification</span>
                                    <input id="F_VCODE" tabIndex="3" type="text" placeholder="Verification Code" value={verificationCode} onChange={(e)=>setVerificationCode(e.target.value)}  className="input_code" />
                                    <a href="" className="code_text">
                                        <img id="val_code" src="https://west-mrms.codm.activision.com/commonAct/codmwest_cdk_center/valCode.php?sServiceType=codmatvi&codeKey=90d040ab7de0509ec808384b9ae970e1" width="138" height="52" alt="code" />
                                    </a>
                                    <a href="" className="icon_break sp" id="refvc">break</a>
                                </li>
                            </ul>
                            <button tabIndex="4" className="btn_confirm sp btn_confirm_gray" id="label_submit" onClick={()=>handleSubmit()}>Submit</button> 
                    </div>
                </div>
            </div>
        </div>
    </>
)
    }
export default Home