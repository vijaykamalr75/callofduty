import express from 'express'
import cors from 'cors'
import router from './routes/index.js'

const app = express()
app.use(cors({
  origin:'http://localhost:3000'
}))
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use('/',router)
app.listen(5000,()=>{
  console.log('app listening on port 5000')
})