import express from 'express'
import request from 'request'
var router = express.Router()


var _BaseUrl="https://west-mrms.codm.activision.com/commonAct/codmwest_cdk_center/"
router.post('/confirmInfo',(req,res)=>{
    try {
      console.log(req.body.body)
        const requestData = req.body.body
        const { fuid, verificationCode  ,redeemCode} = requestData
        const roleId = fuid
        const verifyCode = verificationCode
        const codeKey = redeemCode
        const confirmData =  { sServiceType: 'codmatvi', roleId, sCdk:codeKey, verifyCode, area:1, lang: 'en-us' }
        const options = {
          method: 'GET',
          url: `${_BaseUrl}confirmInfo.php`,
          qs: confirmData,
          headers: {
            'Content-Type': 'application/json'
          },
          dataType: 'jsonp',
          async: true,
          xhrFields: {
            withCredentials: true
          }
        }
        request(options, (async (error, response, confirmbody)=> {
          if (error) {
           res.json({ "iRet": -11101, "data": "Something went wrong" })
          } else if(confirmbody.iRet === 0){
            let exchangeData = await getExchangekey(confirmbody,requestData)
            console.log(exchangeData)
            res.json({ "iRet": 0, data:exchangeData })
          }else
            res.json({ "iRet": 0, data:confirmbody })
        }))
    } catch (error) {
        console.log(error)
        res.json({ "iRet": 0, data:error })
    }
})
const getExchangekey = (confirmInfo, reqbody) => {
  return new Promise((resolve) => {
    try{
    const { verifyKey,roleId } = confirmInfo.jData
    const {  verificationCode,redeemCode } = reqbody
    const sCdk = redeemCode
   const exchangeData =  { sServiceType: 'codmatvi', roleid: roleId, sCdk, verifyKey, areaid: 1, platid: 0,codeKey:verificationCode, partition: 0,lang:'zh-tw' }
    const exchangeOption = {
      method: 'GET',
      url: `${_BaseUrl}ExchangeCdkey.php`,
      qs: exchangeData,
      headers: {
        'Content-Type': 'application/json'
      },
      dataType: 'jsonp',
      async: true,
      xhrFields: {
        withCredentials: true
      }
    }
    request(exchangeOption, function (exerror, exresponse, exbody) {

      if (exerror) {
        return resolve({ "iRet": -11101, "data": "Something went wrong" })
      }
      return resolve(exbody)
    })
  }catch(er){
    console.log(er)
    return resolve({ "iRet": -11101, "data": "Something went wrong" })
  }
  })
}
export default router